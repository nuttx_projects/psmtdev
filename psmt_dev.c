/****************************************************************************
 * psmt_dev.c
 *
 *   Copyright (C) 2019 Matias Nitsche. All rights reserved.
 *   Author: Matias Nitsche <mnitsche@dc.uba.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PSMT nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <poll.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <limits.h>
#include <queue.h>

#include <nuttx/semaphore.h>
#include <nuttx/fs/fs.h>
#include <nuttx/drivers/drivers.h>
#include <nuttx/kmalloc.h>
#include <semaphore.h>

#include <nuttx/module.h>
#include <nuttx/lib/modlib.h>

#include "psmt_dev.h"

/****************************************************************************
 * Private Types
 ****************************************************************************/


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static int     psmt_open(FAR struct file *filep);
static int     psmt_close(FAR struct file *filep);
static ssize_t psmt_read(FAR struct file *filep, FAR char *buffer,
                 size_t buflen);
static ssize_t psmt_write(FAR struct file *filep, FAR const char *buffer,
                 size_t buflen);
static int     psmt_ioctl(FAR struct file *filep, int cmd,
                 unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations psmt_fops =
{
  psmt_open,          /* open */
  psmt_close,         /* close */
  psmt_read,          /* read */
  psmt_write,         /* write */
  NULL,               /* seek */
  psmt_ioctl,         /* ioctl */
  NULL                /* poll */
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL              /* unlink */
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: psmt_open
 *
 * Description: Open the device
 *
 ****************************************************************************/

static int psmt_open(FAR struct file *filep)
{
  UNUSED(filep);
  return OK;
}

/****************************************************************************
 * Name: psmt_close
 *
 * Description: close the device
 *
 ****************************************************************************/

static int psmt_close(FAR struct file *filep)
{
  UNUSED(filep);
  return OK;
}

/****************************************************************************
 * Name: devtopic_read
 ****************************************************************************/

static ssize_t psmt_read(FAR struct file *filep, FAR char *buffer,
                            size_t len)
{
  UNUSED(filep);
  UNUSED(buffer);
  UNUSED(len);

  return OK;
}

/****************************************************************************
 * Name: devtopic_write
 ****************************************************************************/

static ssize_t psmt_write(FAR struct file *filep, FAR const char *buffer,
                             size_t len)
{
  UNUSED(filep);

  return OK;
}

static int psmt_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  int ret = OK;

  UNUSED(filep);
  UNUSED(cmd);
  UNUSED(arg);

  struct psmt_topic_config_t* config = (struct psmt_topic_config_t*)arg;

  ret = psmt_topic_register(config->topic_name, config->message_size);

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: psmt_register
 *
 * Description:
 *   Register a topic
 *
 ****************************************************************************/

int psmt_register(void)
{
  int ret;

  /* create device node */

  ret = register_driver(CONFIG_PSMT_DEV_PATH, &psmt_fops, 0666, NULL);

  return ret;
}
