/****************************************************************************
 * psmt_dev.h
 *
 *   Copyright (C) 2019 Matias Nitsche. All rights reserved.
 *   Author: Matias Nitsche <mnitsche@dc.uba.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PSMT nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_IPC_PSMT_H
#define __INCLUDE_NUTTX_IPC_PSMT_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/input/ioctl.h>
#include <stdbool.h>

#include <signal.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/*
 * IOCTL commands for PSMT _topic_ device
 *
 * Note: these are defined here and may overlap existing NuttX ioctl commands.
 * However, this is not dangerous since the ioctl calls are always mapped to the
 * corresponding descriptor.
 */

#define IOCTL_PSMT_PENDING_COUNT      1   /* arg: size_t*, number of messages in queue */
#define IOCTL_PSMT_TOPIC_INFO         2   /* arg: psmt_topic_info_s* */
#define IOCTL_PSMT_GET_QUEUE_SIZE     3   /* arg: size_t*, size of queue (# messages) */
#define IOCTL_PSMT_SET_QUEUE_SIZE     4   /* arg: size_t*, size of queue (# messages) */
#define IOCTL_PSMT_SET_LOSSLESS       5   /* arg: bool, lossless mode on/off */
#define IOCTL_PSMT_GET_PUBSUB_INFO    6   /* arg: psmt_pubsub_info_list_s* */

/****************************************************************************
 * Public Types
 ****************************************************************************/

/*
 * The structure used as argument to the ioctl() call of the master device, which
 * instantiates a topic device with the given name, under CONFIG_PSMT_DEV_PREFIX
 */

struct psmt_topic_config_t
{
  char topic_name[PATH_MAX];
  size_t message_size;
};

/* Topic information */

struct psmt_topic_info_s
{
  size_t num_subscribers;
  size_t num_publishers;
  size_t message_size;
  bool lossless;
};

/* Subscriber information */

struct psmt_pubsub_info_s
{
  pid_t pid;                  /* Owner of subscriber */
  size_t queue_size;
  size_t dropped_count;
  size_t transfered_count;
};

struct psmt_pubsub_info_list_s
{
  struct psmt_pubsub_info_s *sub_info;
  size_t sub_info_size;

  struct psmt_pubsub_info_s *pub_info;
  size_t pub_info_size;
};

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

int psmt_register(void);

int psmt_topic_register(const char* topic, size_t message_size);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* __INCLUDE_NUTTX_IPC_PSMT_H */
