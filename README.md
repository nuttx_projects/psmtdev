# Publish Subscribe Message Transport (PSMT)

PSMT is a driver and user-library for NuttX, which impements a low-overhead
publish-subscribe message transport, supporting multiple publishers
and multiple subscribers. 

## How does it work

### Low level devices

There are two drivers contained in this repository: a main character driver
(PSMT device) which allows registering new communication topics and a second
character driver (topic device) which implements the communication for a given
topic. This decoupling allows to have a one-to-one mapping between topics
(which are specified as paths, per convention) and device driver nodes under 
the /dev filesystem.

The topic device implements the write operation as a the publish command.
Internally, this implies that the passed data is copied to the internal buffer
and then all tasks (subscribers) waiting on poll() for this particular topic
are notified. Thus, subscribers typically follow the pattern of poll()ing one
or more topics (or even other devices as well), and then receiving newly received
messages using read().

There is a per-subscriber ring-buffer of configurable size. Subscriber
operations are non-blocking (read() returns immediately when nothing left
to read). Publishing, on the other hand, is a blocking operation
(and thus there's no publisher queue).

Operations on a topic are typically "lossy": when a subscribers queue is full,
publishing on this topic will have the effect of dropping old messages from
queue. A topic can also be set to "lossless" mode to guarantee delivery: in
this case the publish operation will block until *all* subscriber's queues
are non-full.

Once all references to a topic device are closed (ie. there are no active
subscribers or publishers of a topic),
the underlying topic device is automatically removed.

### User library

In order to provide a simple and convenient user interface which can be used
from NuttX applications, a C library (libpsmt) is also provided,
which handles all access to the underlying devices.

### Command-line interface

There's a `psmt` application which allows to introspect the PSMT system
from command line. At the moment it is very basic due to the fact that
PSMT is not aware of messages being sent over it (so you cannot
publish/receive messages from command line).
 
## Installation

In order to provide a self-contained package and since NuttX does not easily
support adding external device drivers (while it does support external applications),
this repository is designed to be dropped as an application in NuttX,
while it actually contains the user-level code and the device driver.
The driver will be built as a library and linked into NuttX this way.

Thus, to install and build this package in NuttX, you can create a directory
named "external" under the NuttX apps repo. Inside this directory,
you can place any number of external apps. Inside this directory you should
clone this repository. Finally, if you don't have it already,
add a `Make.defs` file under external with the following contents:

    include $(wildcard external/*/Make.defs)

and a `Makefile` with:

    MENUDESC = "Extra Applications"
    include $(APPDIR)/Directory.mk

Then, go to NuttX config, and go into the new "Extra Applications" menu.
The PSMT config entry should appear and already be selected.
If you just build NuttX, the library and device driver should compile OK. 

The downside of this approach (having the driver as a library) is that this
does not allow you to easily expose the corresponding device initialization 
function to the board implementation code, which needs to be invoked during
NuttX boot (as any other device
driver would). However, since this interface is really simple and you can
simply do the following on your board bringup code. First, copy the
declaration of the `psmt_register()` function from `psmtdev/psmt_dev.h` to
your board code. Then, call this function during board initialization. This
will effectively create the PSMT device, from which everything works.

## Possible improvements

Current implementation has the following limitations:

  * Publishing is a blocking operation: non-blocking publishing can be implemented
  by adding a per-publisher queue that is first filled and then the transmission
  of the messages to the subscriber's queues can be defered using a work queue.
  
# License

The contents of this repository is licensed under the terms of the
BSD three-clause license.
