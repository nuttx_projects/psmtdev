/****************************************************************************
 * psmt_topic_dev.c
 *
 *   Copyright (C) 2019 Matias Nitsche. All rights reserved.
 *   Author: Matias Nitsche <mnitsche@dc.uba.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PSMT nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <poll.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <limits.h>
#include <queue.h>

#include <nuttx/semaphore.h>
#include <nuttx/fs/fs.h>
#include <nuttx/drivers/drivers.h>
#include <nuttx/mm/circbuf.h>
#include <nuttx/kmalloc.h>
#include <semaphore.h>
#include "psmt_dev.h"

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* per-subscriber instance */

struct subscriber_t
{
  struct subscriber_t *flink;
  struct subscriber_t *blink;

  pid_t pid;
  struct circbuf_s buffer;
  struct pollfd* pfd;                    /* to support a polling subscriber */
  sem_t msgsem;                          /* to signal publisher of available buffer space */

  size_t dropped_count;                  /* number of lost messages */
  size_t transfered_count;               /* number of received messages */
};

/* per-publisher instance */

struct publisher_t
{
  struct publisher_t *flink;
  struct publisher_t *blink;

  pid_t pid;
  struct circbuf_s buffer;

  size_t dropped_count;                  /* number of lost messages */
  size_t transfered_count;               /* number of sent messages */
};

struct topic_dev_t
{
  size_t message_size;
  sem_t devsem;                         /* for exclusive access to the device */

  char topic[PATH_MAX];                 /* topic name */

  dq_queue_t subscribers;               /* a reference to all subscribers */
  dq_queue_t publishers;                /* a reference to all publishers */

  size_t num_subscribers;               /* track the number of subscribers (faster access than counting the queue size) */
  size_t num_publishers;                /* ditto for publishers */

  bool lossless;                        /* if message delivery is ensured by waiting for subscribers */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static void psmt_topic_destroy(struct topic_dev_t* dev);

static struct subscriber_t* psmt_topic_add_subscriber(struct topic_dev_t* dev);
static struct subscriber_t* psmt_topic_remove_subscriber(struct topic_dev_t* dev,
                                                         struct subscriber_t* sub);

static struct publisher_t* psmt_topic_add_publisher(struct topic_dev_t* dev, bool use_queue);
static struct publisher_t* psmt_topic_remove_publisher(struct topic_dev_t* dev,
                                                       struct publisher_t* sub);

static void psmt_topic_notify(struct pollfd* fd);

static int     psmt_topic_open(FAR struct file *filep);
static int     psmt_topic_close(FAR struct file *filep);
static ssize_t psmt_topic_read(FAR struct file *filep, FAR char *buffer,
                 size_t buflen);
static ssize_t psmt_topic_write(FAR struct file *filep, FAR const char *buffer,
                 size_t buflen);
static int     psmt_topic_ioctl(FAR struct file *filep, int cmd,
                 unsigned long arg);
static int     psmt_topic_poll(FAR struct file *filep, FAR struct pollfd *fds,
                 bool setup);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations psmt_topic_fops =
{
  psmt_topic_open,          /* open */
  psmt_topic_close,         /* close */
  psmt_topic_read,          /* read */
  psmt_topic_write,         /* write */
  NULL,                     /* seek */
  psmt_topic_ioctl,         /* ioctl */
  psmt_topic_poll           /* poll */
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL                    /* unlink */
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: psmt_topic_open
 *
 * Description: Open the topic device
 *
 ****************************************************************************/

static int psmt_topic_open(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct topic_dev_t *dev   = inode->i_private;
  int ret = OK;

  /* only support either subscriber (read-only) or publisher (write-only) */

  if (!(filep->f_oflags & O_RDONLY) && !(filep->f_oflags & O_WRONLY))
    {
      return -EINVAL;
    }

  /* subscriber is non-blocking, publisher is blocking */

  if ((filep->f_oflags & O_RDONLY && !(filep->f_oflags & O_NONBLOCK)) ||
      (filep->f_oflags & O_WRONLY && (filep->f_oflags & O_NONBLOCK)))
    {
      /* TODO: support publisher non-blocking mode */

      return -EPERM;
    }

  /* get exclusive access */

  ret = nxsem_wait_uninterruptible(&dev->devsem);

  if (ret < 0)
    {
      return ret;
    }

  if (filep->f_oflags & O_RDONLY)
    {
      /* this is a subscriber */

      struct subscriber_t* subscriber = psmt_topic_add_subscriber(dev);

      if (!subscriber)
        {
          ret = -ENOMEM;
        }
      else
        {
          /* associate file with subscriber */

          filep->f_priv = subscriber;
        }
    }
  else
    {
      /* this is a publisher */

      struct publisher_t* publisher =
          psmt_topic_add_publisher(dev, filep->f_oflags & O_NONBLOCK);

      if (!publisher)
        {
          ret = -ENOMEM;
        }
      else
        {
          /* associate file with publisher */

          filep->f_priv = publisher;
        }
    }

  nxsem_post(&dev->devsem);

  return ret;
}

/****************************************************************************
 * Name: psmt_topic_close
 *
 * Description: close the topic device
 *
 ****************************************************************************/

static int psmt_topic_close(FAR struct file *filep)
{
  struct topic_dev_t* dev = (struct topic_dev_t*)filep->f_inode->i_private;
  int ret = OK;

  /* exclusive access */

  ret = nxsem_wait_uninterruptible(&dev->devsem);

  if (ret < 0)
  {
    return ret;
  }

  if (filep->f_oflags & O_RDONLY)
    {
      /* deallocate subscriber */

      psmt_topic_remove_subscriber(dev, (struct subscriber_t*)filep->f_priv);
    }
  else
    {
      /* deallocate publisher */

      psmt_topic_remove_publisher(dev, (struct publisher_t*)filep->f_priv);
    }

  /* if this is the last reference to the device, remove it */

  if (dev->num_publishers + dev->num_subscribers == 0)
    {
      /* should be safe to release the semafore now */

      nxsem_post(&dev->devsem);

      /* destroy all data and unregister driver */

      psmt_topic_destroy(dev);
    }
  else
    {
      nxsem_post(&dev->devsem);
    }

  return ret;
}

/****************************************************************************
 * Name: psmt_topic_read
 ****************************************************************************/

static ssize_t psmt_topic_read(FAR struct file *filep, FAR char *buffer,
                            size_t len)
{
  struct topic_dev_t* dev = (struct topic_dev_t*)filep->f_inode->i_private;
  int ret = OK;

  /* get exclusive access */

  ret = nxsem_wait_uninterruptible(&dev->devsem);

  if (ret < 0)
    {
      return ret;
    }

  if (len != dev->message_size)
    {
      /* require a read size equal to the message size */

      ret = -EINVAL;
    }
  else
    {
      struct subscriber_t* subscriber = (struct subscriber_t*)filep->f_priv;

      /* pass data, if requested */

      if (buffer)
        {
          if (circbuf_is_empty(&subscriber->buffer))
            {
              /* no pending messages */

              ret = -EWOULDBLOCK;
            }
          else
            {
              /* pass on available data */

              circbuf_read(&subscriber->buffer, buffer, dev->message_size);
              ret = dev->message_size;
            }
        }
      else
        {
          /* no buffer supplied, just discard message */

          circbuf_skip(&subscriber->buffer, dev->message_size);
          ret = dev->message_size;
        }

      if (ret > 0)
        {
          subscriber->transfered_count++;
        }

      /* If this is a lossless topic and we have read something,
       * notify publisher of available space.
       */

      if (dev->lossless && ret > 0)
        {
          int semcount;

          nxsem_get_value(&subscriber->msgsem, &semcount);

          if (semcount < 0)
            {
              nxsem_post(&subscriber->msgsem);
            }
        }
    }

  nxsem_post(&dev->devsem);

  return ret;
}

/****************************************************************************
 * Name: psmt_topic_write
 ****************************************************************************/

static ssize_t psmt_topic_write(FAR struct file *filep, FAR const char *buffer,
                             size_t len)
{
  struct topic_dev_t* dev = (struct topic_dev_t*)filep->f_inode->i_private;
  struct publisher_t* pub = (struct publisher_t*)filep->f_priv;
  int ret = OK;

  /* exclusive access */

  ret = nxsem_wait_uninterruptible(&dev->devsem);
  if (ret < 0)
    {
      return ret;
    }

  if (len != dev->message_size)
    {
      ret = -EINVAL;
    }
  else
    {
      if (filep->f_oflags & O_NONBLOCK)
        {
          /* TODO: write to publisher buffer and use work_queue
           * to send messages to subscriber's queues
           */
        }
      else
        {
          /* copy message to all subscribers */

          for (struct subscriber_t* sub = (struct subscriber_t*)dq_peek(&dev->subscribers);
               sub; sub = dq_next(sub))
            {
              if (dev->lossless)
                {
                  /* wait for required buffer space to become available */

                  while (circbuf_is_full(&sub->buffer))
                    {
                      /* unlock topic */

                      nxsem_post(&dev->devsem);

                      /* wait for subscriber to free space */

                      nxsem_wait_uninterruptible(&sub->msgsem);

                      /* lock device again, now we can continue */

                      nxsem_wait_uninterruptible(&dev->devsem);
                    }

                  /* we now have available space */

                  circbuf_write(&sub->buffer, buffer, dev->message_size);
                }
              else
                {
                  /* discard old data as needed */

                  if (circbuf_overwrite(&sub->buffer, buffer, dev->message_size) > 0)
                    {
                      sub->dropped_count++;
                    }
                }
            }

          pub->transfered_count++;

          ret = len;

          /* notify any polling subscribers */

          for (struct subscriber_t* sub = (struct subscriber_t*)dq_peek(&dev->subscribers);
               sub; sub = dq_next(sub))
            {
              /* if this subscriber was polling for a read, notify it */

              if (sub->pfd)
                {
                  psmt_topic_notify(sub->pfd);
                }
            }
        }
    }

  nxsem_post(&dev->devsem);

  return ret;
}

/****************************************************************************
 * Name: psmt_topic_poll
 ****************************************************************************/

static int psmt_topic_poll(FAR struct file *filep, FAR struct pollfd *fds,
                           bool setup)
{
  struct topic_dev_t* dev = (struct topic_dev_t*)filep->f_inode->i_private;
  int ret = OK;

  /* we only support poll for subscribers */

  if (filep->f_oflags & O_WRONLY)
    {
      return -EPERM;
    }

  /* similarly, we only support POLLIN */

  if ((fds->events & POLLOUT) || !(fds->events & POLLIN))
    {
      return -EINVAL;
    }

  /* exclusive access */

  ret = nxsem_wait_uninterruptible(&dev->devsem);

  if (ret < 0)
    {
      return ret;
    }

  struct subscriber_t* sub = (struct subscriber_t*)filep->f_priv;

  if (setup)
    {
      /* associate pollfd to subscriber */

      sub->pfd = fds;
      fds->revents = 0;

      /* if we already have unread messages, notify immediately */

      if (!circbuf_is_empty(&sub->buffer))
        {
          psmt_topic_notify(sub->pfd);
        }
    }
  else
    {
      /* deassociate pollfd to subscriber */

      sub->pfd = NULL;
    }

  nxsem_post(&dev->devsem);

  return ret;
}

/****************************************************************************
 * Name: psmt_topic_poll
 ****************************************************************************/

static int psmt_topic_ioctl(FAR struct file *filep, int cmd,
                 unsigned long arg)
{
  struct topic_dev_t* dev = (struct topic_dev_t*)filep->f_inode->i_private;
  int ret = OK;

  /* get exclusive access */

  ret = nxsem_wait_uninterruptible(&dev->devsem);

  if (ret < 0)
    {
      return ret;
    }

  switch(cmd)
    {
      case IOCTL_PSMT_PENDING_COUNT:
      {
        assert(arg);

        struct circbuf_s *buf;

        if (filep->f_oflags & O_WRONLY)
          {
            struct publisher_t* publisher = (struct publisher_t*)filep->f_priv;
            buf = &publisher->buffer;
          }
        else
          {
            struct subscriber_t* subscriber = (struct subscriber_t*)filep->f_priv;
            buf = &subscriber->buffer;
          }

        *(size_t *)arg = circbuf_used(buf) / dev->message_size;
      }
      break;
      case IOCTL_PSMT_TOPIC_INFO:
      {
        assert(arg);

        struct psmt_topic_info_s *tinfo = (struct psmt_topic_info_s *)arg;

        tinfo->message_size     = dev->message_size;
        tinfo->num_publishers   = dev->num_publishers;
        tinfo->num_subscribers  = dev->num_subscribers;
        tinfo->lossless         = dev->lossless;
      }
      break;
      case IOCTL_PSMT_GET_QUEUE_SIZE:
      {
        assert(arg);

        struct circbuf_s *buf;

        if (filep->f_oflags & O_WRONLY)
          {
            struct publisher_t* publisher = (struct publisher_t*)filep->f_priv;
            buf = &publisher->buffer;
          }
        else
          {
            struct subscriber_t* subscriber = (struct subscriber_t*)filep->f_priv;
            buf = &subscriber->buffer;
          }

        *(size_t *)arg = circbuf_size(buf) / dev->message_size;
      }
      break;
      case IOCTL_PSMT_SET_QUEUE_SIZE:
      {
        assert(arg);

        size_t queue_size = *(size_t *)arg;
        struct circbuf_s *buf;

        if (filep->f_oflags & O_WRONLY)
          {
            if (!(filep->f_oflags & O_NONBLOCK))
              {
                /* in blocking mode, queue is not used */

                ret = -EINVAL;
                goto errout;
              }

            struct publisher_t* publisher = (struct publisher_t*)filep->f_priv;
            buf = &publisher->buffer;
          }
        else
          {
            struct subscriber_t* subscriber = (struct subscriber_t*)filep->f_priv;
            buf = &subscriber->buffer;
          }

        if (queue_size == 0)
          {
            ret = -EINVAL;
          }
        else
          {
            size_t newsize = dev->message_size * queue_size;
            if (circbuf_size(buf) != newsize)
              {
                ret = circbuf_resize(buf, newsize);
              }
          }
      }
      break;
      case IOCTL_PSMT_SET_LOSSLESS:
      {
        dev->lossless = arg;
      }
      break;
      case IOCTL_PSMT_GET_PUBSUB_INFO:
      {
        assert(arg);

        struct subscriber_t* sub;
        struct publisher_t* pub;

        size_t i = 0;

        struct psmt_pubsub_info_list_s *info_list =
            (struct psmt_pubsub_info_list_s *)arg;

        assert(info_list->pub_info_size == 0 || info_list->pub_info);
        assert(info_list->sub_info_size == 0 || info_list->sub_info);

        for (sub = (struct subscriber_t*)dq_peek(&dev->subscribers), i = 0;
             sub && i < info_list->sub_info_size; sub = dq_next(sub))
          {
            info_list->sub_info[i].queue_size = circbuf_size(&sub->buffer);
            info_list->sub_info[i].pid = sub->pid;
            info_list->sub_info[i].transfered_count = sub->transfered_count;
            info_list->sub_info[i].dropped_count = sub->dropped_count;

            i++;
          }

        info_list->sub_info_size = i;

        for (pub = (struct publisher_t*)dq_peek(&dev->publishers), i = 0;
             pub && i < info_list->pub_info_size; pub = dq_next(pub))
          {
            info_list->pub_info[i].queue_size = circbuf_size(&pub->buffer);
            info_list->pub_info[i].pid = pub->pid;
            info_list->pub_info[i].transfered_count = pub->transfered_count;
            info_list->pub_info[i].dropped_count = pub->dropped_count;

            i++;
          }

        info_list->pub_info_size = i;
      }
      break;
      default:
      {
        ret = -EINVAL;
      }
      break;
    }

errout:
  nxsem_post(&dev->devsem);

  return ret;
}

/****************************************************************************
 * Name: psmt_topic_destroy
 ****************************************************************************/

static void psmt_topic_destroy(struct topic_dev_t* dev)
{
  char path[PATH_MAX];

  /* remove driver and free memory */

  snprintf(path, PATH_MAX, CONFIG_PSMT_DEV_PREFIX "%s", dev->topic);
  unregister_driver(path);

  kmm_free(dev);
}

/****************************************************************************
 * Name: psmt_topic_add_subscriber
 ****************************************************************************/

static struct subscriber_t* psmt_topic_add_subscriber(struct topic_dev_t* dev)
{
  struct subscriber_t* subscriber = (struct subscriber_t*)kmm_zalloc(sizeof(struct subscriber_t));

  if (subscriber)
    {
      /* add to list */

      dq_addlast((dq_entry_t*)subscriber, &dev->subscribers);

      /* initialize circular buffer with space for one message */

      circbuf_init(&subscriber->buffer, NULL, dev->message_size);

      /* initialize signaling semaphore */

      sem_init(&subscriber->msgsem, 0, 0);

      /* get pid of owner */

      subscriber->pid = getpid();

      /* increment subscriber count */

      dev->num_subscribers++;
    }

  return subscriber;
}

/****************************************************************************
 * Name: psmt_topic_remove_subscriber
 ****************************************************************************/

static struct subscriber_t* psmt_topic_remove_subscriber(struct topic_dev_t* dev,
                                                         struct subscriber_t* sub)
{
  struct subscriber_t* next = dq_next(sub);

  dq_rem((dq_entry_t*)sub, &dev->subscribers);

  circbuf_uninit(&sub->buffer);

  kmm_free(sub);

  dev->num_subscribers--;

  return next;
}

/****************************************************************************
 * Name: psmt_topic_add_publisher
 ****************************************************************************/

static struct publisher_t* psmt_topic_add_publisher(struct topic_dev_t* dev,
                                                    bool use_queue)
{
  struct publisher_t* publisher =
      (struct publisher_t*)kmm_zalloc(sizeof(struct publisher_t));

  if (publisher)
    {
      /* add to list */

      dq_addlast((dq_entry_t*)publisher, &dev->publishers);

      if (use_queue)
        {
          /* initialize circular buffer with space for one message */

          circbuf_init(&publisher->buffer, NULL, dev->message_size);
        }

      /* get pid of owner */

      publisher->pid = getpid();

      /* increment publisher count */

      dev->num_publishers++;
    }

  return publisher;
}

/****************************************************************************
 * Name: psmt_topic_remove_publisher
 ****************************************************************************/

static struct publisher_t* psmt_topic_remove_publisher(struct topic_dev_t* dev,
                                                        struct publisher_t* pub)
{
  struct publisher_t* next = dq_next(pub);

  dq_rem((dq_entry_t*)pub, &dev->publishers);

  if (circbuf_size(&pub->buffer) != 0)
    {
      circbuf_uninit(&pub->buffer);
    }

  kmm_free(pub);

  dev->num_publishers--;

  return next;
}

/****************************************************************************
 * Name: psmt_topic_notify
 ****************************************************************************/

static void psmt_topic_notify(struct pollfd* fd)
{
  fd->revents |= POLLIN;

  irqstate_t flags;
  int semcount;

  /* Limit the number of times that the semaphore is posted.
   * The critical section is needed to make the following
   * operation atomic.
   */

  flags = enter_critical_section();

  nxsem_get_value(fd->sem, &semcount);
  if (semcount < 1)
    {
      nxsem_post(fd->sem);
    }

  leave_critical_section(flags);
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: psmt_topic_register
 *
 * Description:
 *   Register a topic
 *
 ****************************************************************************/

int psmt_topic_register(const char* topic, size_t message_size)
{
  struct topic_dev_t* dev = NULL;
  int ret = OK;
  char path[PATH_MAX];

  if (message_size == 0)
  {
    return -EINVAL;
  }

  /* build output path from topic path */

  if (snprintf(path, PATH_MAX, CONFIG_PSMT_DEV_PREFIX "%s", topic) >= PATH_MAX)
  {
    /* supplied topic is too long */

    return -EINVAL;
  }

  /* allocate instance structure */

  dev = (struct topic_dev_t*)kmm_zalloc(sizeof(struct topic_dev_t));

  if (!dev)
  {
    return -ENOMEM;
  }

  nxsem_init(&dev->devsem, 0, 1);

  dev->message_size = message_size;
  strncpy(dev->topic, topic, PATH_MAX);

  /* create device node */

  ret = register_driver(path, &psmt_topic_fops, 0666, dev);

  if (ret < 0)
  {
    kmm_free(dev);
  }

  return ret;
}
